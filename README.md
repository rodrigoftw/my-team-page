# My Team Page #

## Solution for a challenge from [Devchallenges.io](http://devchallenges.io). ##


## [Demo](https://youthful-montalcini-6defdb.netlify.app/) | [Solution](https://devchallenges.io/solutions/iaCXcDr7qXllFExDJh89) | [Challenge](https://devchallenges.io/challenges/hhmesazsqgKXrTkYkt0U) ##

## Table of Contents

- [Overview](#overview)
- [Built With](#built-with)
- [Features](#features)
- [Contact](#contact)

## Overview

![screenshot](https://devchallenges.io/_next/image?url=https%3A%2F%2Ffirebasestorage.googleapis.com%2Fv0%2Fb%2Fdevchallenges-1234.appspot.com%2Fo%2FchallengesDesigns%252FmyTeamThumbnail.png%3Falt%3Dmedia%26token%3D3dc1bba0-52b9-440d-8a54-fa31ec7c84d8&w=1920&q=75)

This application/site was created as a submission to a [DevChallenges](https://devchallenges.io/challenges) challenge.
The [challenge](https://devchallenges.io/challenges/hhmesazsqgKXrTkYkt0U) was to build an application to complete the given user stories.

## Built With

- [HTML](https://html.spec.whatwg.org/)
- [CSS](https://www.w3.org/Style/CSS/Overview.en.html)

## Features

This project features a desktop view and a mobile view of the page.


## Contact

You can contact me [here](https://linktr.ee/rodrigodev).
